from os import environ

from jps_api_wrapper.classic import Classic


JPS_URL = environ["JPS_URL"]
JPS_USERNAME = environ["JPS_USERNAME"]
JPS_PASSWORD = environ["JPS_PASSWORD"]

try:
    MOBILE_DEVICE_CONFIG_PROFILE_NAME = environ["MOBILE_DEVICE_CONFIG_PROFILE_NAME"]
except KeyError:
    MOBILE_DEVICE_CONFIG_PROFILE_NAME = None
try:
    COMPUTER_CONFIG_PROFILE_NAME = environ["COMPUTER_CONFIG_PROFILE_NAME"]
except KeyError:
    COMPUTER_CONFIG_PROFILE_NAME = None


class NoConfigProfilesDefined(Exception):
    """
    Raised when neither MOBILE_DEVICE_CONFIG_PROFILE_NAME or
    COMPUTER_CONFIG_PROFILE_NAME are passed in as environment variables.
    """


class MultipleConfigProfilesDefined(Exception):
    """
    Raised when MOBILE_DEVICE_CONFIG_PROFILE_NAME and
    COMPUTER_CONFIG_PROFILE_NAME are passed in as environment variables.
    """


def main():
    if not MOBILE_DEVICE_CONFIG_PROFILE_NAME and not COMPUTER_CONFIG_PROFILE_NAME:
        raise NoConfigProfilesDefined(
            """
            Need to set at least one config profile name.

            This can be done by setting either of the following to the name of a
            config profile:
            MOBILE_DEVICE_CONFIG_PROFILE_NAME
            COMPUTER_CONFIG_PROFILE_NAME
            """
        )
    if MOBILE_DEVICE_CONFIG_PROFILE_NAME and COMPUTER_CONFIG_PROFILE_NAME:
        raise MultipleConfigProfilesDefined(
            """
            Please only define either MOBILE_DEVICE_CONFIG_PROFILE_NAME or 
            COMPUTER_CONFIG_PROFILE_NAME but not both in the same run.
            """
        )

    with Classic(JPS_URL, JPS_USERNAME, JPS_PASSWORD) as classic:
        if MOBILE_DEVICE_CONFIG_PROFILE_NAME:
            data = """
            <configuration_profile>
                <general>
                    <redeploy_on_update>All</redeploy_on_update>
                </general>
            </configuration_profile>
            """
            classic.update_mobile_device_configuration_profile(
                data, name=MOBILE_DEVICE_CONFIG_PROFILE_NAME
            )
            print(
                f"Mobile device config profile {MOBILE_DEVICE_CONFIG_PROFILE_NAME} "
                "redeployed to scoped devices."
            )

        if COMPUTER_CONFIG_PROFILE_NAME:
            data = """
            <os_x_configuration_profile>
                <general>
                    <redeploy_on_update>All</redeploy_on_update>
                </general>
            </os_x_configuration_profile>"""
            classic.update_osx_configuration_profile(
                data, name=COMPUTER_CONFIG_PROFILE_NAME
            )
            print(
                f"Computer configuration profile {COMPUTER_CONFIG_PROFILE_NAME} "
                "redeployed to scoped devices."
            )


if __name__ == "__main__":
    main()
