JPS Configuration Profile Redeployer
=======================================
Redeploys either a mobile device or computer configuration profile to its scoped assets.


## Reasoning for script
We wanted a way to schedule redeploys of certain config profiles like not autojoinging our public WiFi. This isn't currently supported by Jamf.


## Usage

### Local
Export the variables listed below into your environment:
* All variable values are described below.
* The first command will make your bash history ignore commands with whitespace infront of them. This will make it so your JPS_PASSWORD isn't in 
your terminal history.
```console
setopt HIST_IGNORE_SPACE
export JPS_URL=https://example.jamfcloud.com
export JPS_USERNAME=exampleUsername
 export JPS_PASSWORD=examplePassword
export MOBILE_DEVICE_CONFIG_PROFILE_NAME="Example Name"
```

Run these commands to install all dependencies and run the script:
```console
pip install pipenv
pipenv install --deploy
pipenv run python ./jps_redeploy_configuration_profile.py
```

### GitLab
1. Set the JPS_USERNAME and JPS_PASSWORD as CI/CD variables
2. Create a new CI/CD schedule with JPS_URL and either MOBILE_DEVICE_CONFIG_PROFILE_NAME or COMPUTER_CONFIG_PROFILE_NAME as variables
3. Set the schedule to how often you would like the config profile to redeploy


## Variables

### JPS_URL
* URL of your JPS Server
> https://example.jamfcloud.com

### JPS_USERNAME / JPS_PASSWORD
JPS account credentials with the following permissions set:
* Jamf Pro Server Objects
    * macOS Configuration Profiles
        * Update
    * Mobile Device Configuration Profiles
        * Update

### MOBILE_DEVICE_CONFIG_PROFILE_NAME
Name of the mobile device configuration profile that you want to redeploy.

### COMPUTER_DEVICE_CONFIG_PROFILE_NAME
Name of the computer configuration profile that you want to redeploy.

## Authors

Bryan Weber (bweber26@cvtc.edu)

## Copyright

JPS Configuration Profile Redeployer is Copyright 2023 Chippewa Valley Technical College. It is provided under the MIT License. For more information, please see the LICENSE file in this repository.
